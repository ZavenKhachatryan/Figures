﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Tables
{
    class TableNxN : TableNxM
    {
        private byte column;
        private byte row;

        public override byte Column
        {
            get { return column; }
            set
            {
                column = value;
                row = value;
            }
        }

        public override byte Row
        {
            get { return row; }
            set
            {
                row = value;
                column = value;
            }
        }
    }
}

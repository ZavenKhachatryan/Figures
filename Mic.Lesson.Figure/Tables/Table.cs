﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mic.Lesson.Figure.Shapes;

namespace Mic.Lesson.Figure.Tables
{
    abstract class Table : Shape
    {
        public virtual byte Column { get; set; }
        public virtual byte Row { get; set; }
    }
}

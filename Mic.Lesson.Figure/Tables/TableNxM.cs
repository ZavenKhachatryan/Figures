﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mic.Lesson.Figure.Shapes;

namespace Mic.Lesson.Figure.Tables
{
    class TableNxM : Table
    {
        public override void Draw()
        {
            for (int i = 0; i < Row; i++)
            {
                Console.WriteLine(new string('*', (Width * Column) - Column + 1));
                for (int k = 0; k < Height; k++)
                {
                    for (int j = 0; j < Column; j++)
                        Console.Write($"*{new string(' ', (Width) - 2)}");
                    Console.Write("*");
                    Console.WriteLine();
                }
            }
            Console.WriteLine(new string('*', (Width * Column) - Column + 1));
        }
    }
}
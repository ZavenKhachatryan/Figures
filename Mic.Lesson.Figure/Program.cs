﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mic.Lesson.Figure.Tables;

namespace Mic.Lesson.Figure
{
    class Program
    {
        static void Main(string[] args)
        {
            Table t = new TableNxM();
            t.Width = 12;
            t.Height = 3;
            t.Row = 1;
            t.Column = 1;
            t.Draw();
            Console.ReadLine();
        }
    }
}
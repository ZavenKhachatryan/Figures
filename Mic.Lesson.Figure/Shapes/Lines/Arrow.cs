﻿using System;

namespace Mic.Lesson.Figure.Shapes.Lines
{
    class Arrow : Line
    {
        public override void Draw()
        {
            base.Draw();
            if(IsHorizontale)
                Console.Write(">");
            else
                Console.WriteLine("V");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes.Lines
{
    class Line : Shape
    {
        private byte height;
        public sealed override byte Height
        {
            get { return height; }
            set
            {
                height = value;
                width = 0;
            }
        }
        private byte width;
        public sealed override byte Width
        {
            get { return width; }
            set
            {
                width = value;
                height = 0;
            }
        }

        public bool IsHorizontale => height == 0;

        public override void Draw()
        {
            if(IsHorizontale)
                Console.Write(new string('-', width));
            else
            {
                for (int i = 0; i < height; i++)
                    Console.WriteLine("|");
            }
        }
    }
}
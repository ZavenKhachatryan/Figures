﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes.Lines
{
    class DoubleArrow : Arrow
    {
        public override void Draw()
        {
            if (IsHorizontale)
                Console.Write("<");
            else
                Console.WriteLine("^");
            base.Draw();
        }
    }
}

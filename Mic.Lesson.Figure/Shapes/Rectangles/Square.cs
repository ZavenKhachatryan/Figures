﻿namespace Mic.Lesson.Figure.Shapes.Rectangles
{
    class Square : Rectangle
    {
        private byte height;
        private byte width;

        public override byte Height
        {
            get { return height; }
            set
            {
                height = value;
                width = value;
            }
        }

        public override byte Width
        {
            get { return width; }
            set
            {
                width = value;
                height = value;
            }
        }
    }
}
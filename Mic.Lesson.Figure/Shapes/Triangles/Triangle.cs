﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figure.Shapes.Triangles
{
    class Triangle : Shape
    {
        public override void Draw()
        {
            byte removed = 0;
            for (int h = 0; h < Height; h++)
            {
                Console.WriteLine(new string('*', Width - removed));
                removed++;
            }
        }
    }
}
